# Minecraft Physics Mods

Minecraft Phyiscs Mods (MPM) use Excel worksheets and VBScripts to procedurally generate commands in Minecraft to investigate physics principles and technical applications.

Mods available:

* Soil Sampling (`soil-sampling/MPM_soil-sampling.xlsm`)


Please refer to the individual README files within each mod project for usage.


## Roadmap

**Soil Sampling Mod v1.0**
  - [x] Implement alternative normal distribution function
  - [ ] Complete numerical contamination model component
    - [x] Determine book commands for generating in brownfield
    - [ ] QC and modify functional forms of numerical model
  - [ ] Finish layout, cell interactions in Excel worksheet

**Soil Sampling Mod v2.0**
  - [ ] Create data output tab for randomly generated models
    - [ ] Tabular output
    - [ ] Graphical output

See the [open issues](https://gitlab.com/radiohacktive/minecraft-physics-mods/-/issues) for a full list of proposed features (and known issues).


## Changelog

20220908 Specify roadmap versioning
20220907 Adding changelog, contributing, another correction
20220906 Separating mod readme out (draft), minor corrections
20220906 Documentation, extracting VBA script into vbs modules, normal dist function update
20220905 Initial commit with latest mod file


## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request## License

Distributed under the MIT License. See `LICENSE.txt` for more information.


## Contact

[Radiohacktive](https://radiohacktive.net) - [@Radiohacktivate](https://twitter.com/radiohacktivate) - [Radiohacktivity (Twitch)](https://twich.tv/radiohacktivity)

Project Link: [https://gitlab.com/radiohacktive/minecraft-physics-mods](https://gitlab.com/radiohacktive/minecraft-physics-mods)
