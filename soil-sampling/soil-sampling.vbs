' Obtain world, function namespace, and folder names from Settings tab
Dim scriptdir: scriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
Set xlBook = GetObject("C:\Users\Radiohacktive\Programs\GitLab\minecraft-physics-mods\MPM_soil-sampling.xlsm")
Set xlApp = xlBook.Application


Function RandNorm(mean, sd)

    ' Create temp vars
    Dim r1, r2, s
    
    ' Get nonzero random number
    r1 = Rnd()
    If r1 = 0 Then r1 = Rnd()
    r2 = Rnd()
    s = Sqr(-2*Log(r1)) * Cos(6.283185307 * r2)
    RandNorm = mean + sd * s

End Function


Function mathmin(a, b)

    If a < b Then
        mathmin = a
    Else
        mathmin = b
    End If

End Function


Function mathmax(a, b)

    If a > b Then
        mathmax = a
    Else
        mathmax = b
    End If

End Function


Function createMCFunctionFile(mcfunctionfilename, mcfunctioncommand)
    
    ' Build function file path
    Dim mcfunctionspath: mcfunctionspath = xlApp.Range("mcfunctionspath").value

    ' Create and assign file system object at file path
    Dim FSO: Set FSO = CreateObject("Scripting.FileSystemObject")
    Dim outputFileName: outputFileName = mcfunctionspath + "\" + mcfunctionfilename
    Dim outputFile: Set outputFile = FSO.CreateTextFile(outputFileName, True)
    
    ' Open contamination.mcfunction for writing
    outputFile.WriteLine (mcfunctioncommand)
    MsgBox(mcfunctioncommand)

    ' End function file writing
    Set FSO = Nothing
    
End Function


Function commandClearArea(blocks)
    
    ' Define blocks used
    Dim grassBlock: grassBlock = blocks("Grass")
    Dim airBlock: airBlock = blocks("Air")
    
    ' Define area and depth
    Dim areaSize: areaSize = xlApp.Range("areasize").value
    Dim areaDepth: areaDepth = xlApp.Range("areadepth").value
    
    ' Clear 2.5Nx2.5N area around user (where N is areaSize)
    ' (Essentially, allow the user to reset the area from within a radius of the outside edge)
    Dim areaClearSize: areaClearSize = areaSize * 2.5 ' Must be odd to surround user neatly

    ' Define boundary coordinates
    Dim x1: x1 = Int((areaClearSize - 1) / 2)
    Dim z1: z1 = Int((areaClearSize - 1) / 2)
    Dim x2: x2 = Int((areaClearSize - 1) / 2)
    Dim y2: y2 = Int(areaDepth)
    Dim z2: z2 = Int((areaClearSize - 1) / 2)

    ' Build grass-replacement command
    Dim mcfunctioncommand
    mcfunctioncommand = "fill " + "~-" + CStr(x1) + " ~-1 " + "~-" + CStr(z1) _
                        + " ~" + CStr(x2) + " ~-" + CStr(y2) + " ~" + CStr(z2) _
                        + " " + grassBlock + " replace" + vbCrLf

    ' Build air clearing command
    mcfunctioncommand = mcfunctioncommand + "fill " + "~-" + CStr(x1) + " ~ " + "~-" + CStr(z1) _
                        + " ~" + CStr(x2) + " ~" + CStr(y2) + " ~" + CStr(z2) _
                        + " " + airBlock + " replace"
    
    ' Minecraft function file
    Dim mcfunctionfilename: mcfunctionfilename = "cleararea.mcfunction"
    Call createMCFunctionFile(mcfunctionfilename, mcfunctioncommand)
    
End Function


Function commandCreateArea(blocks, analyteBlocks, analytes, analyteNames, analyteMaxes, analyteStdevs, analyteBkgs, analyteBkgStdevs):
    
    ' Define blocks used
    Dim grassBlock: grassBlock = blocks("Grass")
    Dim brownfieldBlock: brownfieldBlock = blocks("Brownfield")
    Dim airBlock: airBlock = blocks("Air")
    Dim analyteBox: analyteBox = blocks("AnalyteBox")
    
    '' Define base brownfield area
    ' Define area and depth
    Dim areaSize: areaSize = xlApp.Range("areasize").value
    Dim areaDepth: areaDepth = xlApp.Range("areadepth").value
    '
    ' Define boundary coordinates
    Dim x1: x1 = Int((areaSize - 1) / 2)
    Dim z1: z1 = Int((areaSize - 1) / 2)
    Dim x2: x2 = Int((areaSize - 1) / 2)
    Dim y2: y2 = Int(areaDepth)
    Dim z2: z2 = Int((areaSize - 1) / 2)
    '
    ' Build brownfield command
    Dim mcfunctioncommand
    mcfunctioncommand = mcfunctioncommand + "fill " _
                        + "~-" + CStr(x1) + " ~-1 " + "~-" + CStr(z1) _
                        + " ~" + CStr(x2) + " ~-" + CStr(y2) + " ~" + CStr(z2) _
                        + " " + brownfieldBlock + " replace" + vbCrLf
                        
                        
    '' Define discrete contamination
    ' Generates location and size of contamination in brownfield
    Dim numberofveins: numberofveins = 1
    '
    ' Get # of analytes
    Dim numAnalytes: numAnalytes = UBound(analyteNames)
    '
    ' Provide initial vein coordinates, (X0(#), Y0(#), Z0(#))
    Dim veinXinit(), veinYinit(), veinZinit()
    Dim veinXGrowth(), veinYGrowth(), veinZGrowth()
    Dim discretemcfunctioncommand: discretemcfunctioncommand = mcfunctioncommand
    For analyteNum = 1 To numAnalytes
    
        ' Dynamically redimensionalize
        ReDim Preserve veinXinit(analyteNum)
        ReDim Preserve veinYinit(analyteNum)
        ReDim Preserve veinZinit(analyteNum)
        ReDim Preserve veinXGrowth(analyteNum)
        ReDim Preserve veinYGrowth(analyteNum)
        ReDim Preserve veinZGrowth(analyteNum)
    
        Randomize: veinXinit(analyteNum) = Int(areaSize * Rnd) - ((areaSize - 1) / 2)
        Randomize: veinYinit(analyteNum) = -(Int(areaDepth * Rnd + 1))
        Randomize: veinZinit(analyteNum) = Int(areaSize * Rnd) - ((areaSize - 1) / 2)
        '
        ' Enter vein coordinates into output cells
        xlApp.Range("posX" + CStr(analyteNum)).value = CStr(veinXinit(analyteNum))
        xlApp.Range("posY" + CStr(analyteNum)).value = CStr(veinYinit(analyteNum))
        xlApp.Range("posZ" + CStr(analyteNum)).value = CStr(veinZinit(analyteNum))
        '
        ' Define vein growth size (beyond 1 in each direction as random number from an exp probability distribution,
        ' P(beta) = e^(-x/beta), so decreasing probability of larger vein growth
        'Dim scaleX: scaleX = 0.25
        'Dim scaleY: scaleY = 0.25
        'Dim scaleZ: scaleZ = 0.25
        Dim charLength: charLength = 3.5
        Randomize: veinXGrowth(analyteNum) = Int(charLength * Exp(-Rnd))
        Randomize: veinYGrowth(analyteNum) = Int(charLength * Exp(-Rnd))
        Randomize: veinZGrowth(analyteNum) = Int(charLength * Exp(-Rnd))
        '
        ' Enter vein sizes into output cells
        xlApp.Range("sizeX" + CStr(analyteNum)).value = CStr(1 + 2 * veinXGrowth(analyteNum))
        xlApp.Range("sizeY" + CStr(analyteNum)).value = CStr(1 + 2 * veinYGrowth(analyteNum))
        xlApp.Range("sizeZ" + CStr(analyteNum)).value = CStr(1 + 2 * veinZGrowth(analyteNum))
        '
        ' Generates discrete contamination region in brownfield
        ' Create fill coordinates that are within the confines of the brownfield
        Dim fillXo: fillXo = CStr(mathmax(veinXinit(analyteNum) - veinXGrowth(analyteNum), -areaSize / 2))
        Dim fillX: fillX = CStr(mathmin(veinXinit(analyteNum) + veinXGrowth(analyteNum), areaSize / 2))
        Dim fillYo: fillYo = CStr(mathmax(veinYinit(analyteNum) - veinYGrowth(analyteNum), -areaDepth))
        Dim fillY: fillY = CStr(mathmin(veinYinit(analyteNum) + veinYGrowth(analyteNum), -1))
        Dim fillZo: fillZo = CStr(mathmax(veinZinit(analyteNum) - veinZGrowth(analyteNum), -areaSize / 2))
        Dim fillZ: fillZ = CStr(mathmin(veinZinit(analyteNum) + veinZGrowth(analyteNum), areaSize / 2))
        '
        ' Compile command string
        discretemcfunctioncommand = discretemcfunctioncommand + "fill " _
                            + "~" + fillXo + " ~" + fillYo + " ~" + fillZo _
                            + " ~" + fillX + " ~" + fillY + " ~" + fillZ _
                            + " " + analyteBlocks(analyteNum) + "{display:{Name:'{""text"":""" + analyteNames(analyteNum) + """}'}} replace" + vbCrLf
    Next 'analyteNum
    '
    ' Minecraft function file for discrete contamination
    Dim mcfunctionfilename: mcfunctionfilename = "discretebrownfield.mcfunction"
    Call createMCFunctionFile(mcfunctionfilename, discretemcfunctioncommand)
    
    
    '' Define continuous and numerical contaminations
    ' Generates continuous contamination region in brownfield
    Dim continuousmcfunctioncommand: continuousmcfunctioncommand = mcfunctioncommand
    Dim numericalmcfunctioncommand: numericalmcfunctioncommand = mcfunctioncommand
    '
    ' Loop through coordinates of brownfield
    Dim areaX, areaY, areaZ, thisCount 'As Integer
    For areaX = -x1 To x1
        For areaY = -y2 To -1
            For areaZ = -z1 To z1
                Dim slotNum: slotNum = 0
                Dim contaminationBookText: contaminationBookText = ""
                Dim contaminationSlots: contaminationSlots = ""
                For analyteNum = 1 To numAnalytes
                    '
                    ' Add background count value for this block- uniform between 1 and maxBGCount
                    Randomize
                    'analyteCount = Int(1 + (analyteBkgs(analyteNum) * Rnd))
                    ' Add background count value for this block- gaussian distribution
                    'analyteCount = CreateObject("Excel.Application").WorksheetFunction.NormInv(Rnd, analyteBkgs(analyteNum), analyteBkgStdevs(analyteNum))
                    analyteCount = RandNorm(analyteBkgs(analyteNum), analyteBkgStdevs(analyteNum))
                    '
                    ' If the block is within that discrete region, fill block with analyteMax counts.
                    'If (areaX <= veinXinit(analyteNum) + veinXGrowth(analyteNum)) And (areaX >= veinXinit(analyteNum) - veinXGrowth(analyteNum)) And _
                    '    (areaY <= veinYinit(analyteNum) + veinYGrowth(analyteNum)) And (areaY >= veinYinit(analyteNum) - veinYGrowth(analyteNum)) And _
                    '    (areaZ <= veinZinit(analyteNum) + veinZGrowth(analyteNum)) And (areaZ >= veinZinit(analyteNum) - veinZGrowth(analyteNum)) Then
                    '
                    ' Generate amount from source based on Gaussian distribution
                    Dim gaussianCount: gaussianCount = analyteMaxes(analyteNum) * Exp(-0.5 * (((areaX - veinXinit(analyteNum)) ^ 2 / veinXGrowth(analyteNum) ^ 2) + ((areaY - veinYinit(analyteNum)) ^ 2 / veinYGrowth(analyteNum) ^ 2) + ((areaZ - veinZinit(analyteNum)) ^ 2 / veinZGrowth(analyteNum) ^ 2)))
                    '
                    ' Add contamination to the box
                    'analyteCount = analyteCount + analyteMaxes(analyteNum)
                    analyteCount = Int(analyteCount + gaussianCount)
                    '
                    ' Build inventory of contamination
                    Do While analyteCount > 64
                        contaminationSlots = contaminationSlots + "{id:""" + analytes(analyteNum) + """,tag:{display:{Name:'{""text"":""" + analyteNames(analyteNum) + """}'}},Count:64,Slot:" + CStr(slotNum) + "},"
                        analyteCount = analyteCount - 64
                        slotNum = slotNum + 1
                    Loop
                    contaminationSlots = contaminationSlots + "{id:""" + analytes(analyteNum) + """,tag:{display:{Name:'{""text"":""" + analyteNames(analyteNum) + """}'}},Count:" + CStr(analyteCount) + ",Slot:" + CStr(slotNum) + "}"
                    '
                    ' This will break if there can be none of a final analyte, for now all have min background of 1
                    If analytNum < numAnalytes Then
                        contaminationSlots = contaminationSlots + ","
                    End If
                    '
                    slotNum = slotNum + 1
                    
                    ' Add analyte value string to contaminationBookText
                    contaminationBookText = contaminationBookText + analyteNames(analyteNum) + ": " + CStr(analyteCount) + "\\n"
                Next 'analyteNum
                '
                ' Add analyte values to the lab report book page
                contaminationBook = "{id:""minecraft:written_book"",Count:1,tag:{title:""Lab Report"",author:""gamma spec"",pages:[""\""" + contaminationBookText + "\""""]}}"
                '
                ' Add counts of contamination item to a barrel
                Dim continuousContaminatedbox, numericalContaminatedbox
                continuousContaminatedbox = analyteBox + "{Items:[" + contaminationSlots + "]}"
                numericalContaminatedbox = analyteBox + "{Items:[" + contaminationBook + "]}"
                '
                '
                ' Convert to string and remove if zero
                Dim strX: strX = ""
                Dim strY: strY = ""
                Dim strZ: strZ = ""
                If areaX <> 0 Then
                    strX = CStr(areaX)
                End If
                If areaY <> 0 Then
                    strY = CStr(areaY)
                End If
                If areaZ <> 0 Then
                    strZ = CStr(areaZ)
                End If
                strX = Trim(strX)
                strY = Trim(strY)
                strZ = Trim(strZ)
                continuousmcfunctioncommand = continuousmcfunctioncommand + "setblock ~" _
                                        + strX + " ~" + strY + " ~" + strZ _
                                        + " " + continuousContaminatedbox + " replace" + vbCrLf
                numericalmcfunctioncommand = numericalmcfunctioncommand + "setblock ~" _
                                        + strX + " ~" + strY + " ~" + strZ _
                                        + " " + numericalContaminatedbox + " replace" + vbCrLf
            Next 'areaZ
        Next 'areaY
    Next 'areaX
    '
    ' Minecraft function file for continuous and numerical contaminations
    mcfunctionfilename = "continuousbrownfield.mcfunction"
    Call createMCFunctionFile(mcfunctionfilename, continuousmcfunctioncommand)
    mcfunctionfilename = "numericalbrownfield.mcfunction"
    Call createMCFunctionFile(mcfunctionfilename, numericalmcfunctioncommand)
    
End Function


Function commandXRayBrownfield(blocks)

    ' Define blocks used
    Dim brownfieldBlock: brownfieldBlock = blocks("Brownfield")
    Dim xrayBlock: xrayBlock = blocks("XRay")
    
    ' Define area and depth
    Dim areaSize: areaSize = xlApp.Range("areasize").value
    Dim areaDepth: areaDepth = xlApp.Range("areadepth").value
    
    ' Xray 3Nx3N area of brownfield around user
    ' Define brownfield replacementparameters
    Dim brownfieldsize: brownfieldsize = areaSize * 2.5 ' Must be odd to surround user neatly
    Dim brownfielddepth: brownfielddepth = areaDepth + 1

    ' define boundary coordinates
    Dim x1: x1 = Int((brownfieldsize - 1) / 2)
    Dim z1: z1 = Int((brownfieldsize - 1) / 2)
    Dim x2: x2 = Int((brownfieldsize - 1) / 2)
    Dim y2: y2 = Int(brownfielddepth - 1)
    Dim z2: z2 = Int((brownfieldsize - 1) / 2)

    ' Build brownfield xray-ing command
    Dim mcfunctioncommand: mcfunctioncommand = "fill " _
                            & "~-" & CStr(x1) & " ~-1 " + "~-" & CStr(z1) _
                            & " ~" & CStr(x2) & " ~-" + CStr(y2) & " ~" + CStr(z2) _
                            & " " & xrayBlock & " replace " & brownfieldBlock

    ' Minecraft function file
    Dim mcfunctionfilename: mcfunctionfilename = "xraybrownfield.mcfunction"
    Call createMCFunctionFile(mcfunctionfilename, mcfunctioncommand)
    
End Function


Function commandItemSoilProbe(blocks):

    ' Define blocks used
    Dim commandBlock: commandBlock = blocks("Command")
    
    ' Define area and depth
    Dim areaDepth: areaDepth = xlApp.Range("areadepth").value

    ' Build boring command ('move' argument replaces original blocks w/ air after)
    ' This will go into a command block
    Dim extractboringcommand
    extractboringcommand = "clone ~ ~-1 ~ ~ ~-" + CStr(areaDepth) + " ~ ~ ~1 ~ replace force"
    
    ' Build command to give command block with boring extraction command to player
    Dim mcfunctioncommand
    mcfunctioncommand = "give @p " + commandBlock + "{BlockEntityTag:{Command:""" + extractboringcommand + """}}"

    ' Minecraft function file
    Dim mcfunctionfilename: mcfunctionfilename = "soilprobe.mcfunction"
    Call createMCFunctionFile(mcfunctionfilename, mcfunctioncommand)

End Function


' Define number of analytes
Dim numAnalytes: numAnalytes = 6

' Define Minecraft blocks and items for analyte blocks and units
Dim analyteBlocks(6), analytes(6)
analyteBlocks(1) = "minecraft:red_wool"
analyteBlocks(2) = "minecraft:orange_wool"
analyteBlocks(3) = "minecraft:yellow_wool"
analyteBlocks(4) = "minecraft:green_wool"
analyteBlocks(5) = "minecraft:blue_wool"
analyteBlocks(6) = "minecraft:purple_wool"
analytes(1) = "minecraft:red_dye"
analytes(2) = "minecraft:orange_dye"
analytes(3) = "minecraft:yellow_dye"
analytes(4) = "minecraft:green_dye"
analytes(5) = "minecraft:blue_dye"
analytes(6) = "minecraft:purple_dye"

' Define analyte input matrix
Dim analyteNames(6), analyteMaxes(6), analyteStdevs(6), analyteBkgs(6), analyteBkgStdevs(6)
Dim analyteNum
For analyteNum = 1 To numAnalytes
    analyteNames(analyteNum) = xlApp.Range("analyte" + CStr(analyteNum)).value
    analyteMaxes(analyteNum) = xlApp.Range("analyteMax" + CStr(analyteNum)).value
    analyteStdevs(analyteNum) = xlApp.Range("analyteStdev" + CStr(analyteNum)).value
    analyteBkgs(analyteNum) = xlApp.Range("analyteBkg" + CStr(analyteNum)).value
    analyteBkgStdevs(analyteNum) = xlApp.Range("analyteBkgStdev" + CStr(analyteNum)).value
Next 'analyteNum

' Define Minecraft blocks for representations of soil sampling
Dim blocks: Set blocks = CreateObject("Scripting.Dictionary")
blocks.Add "Air", "minecraft:air"
blocks.Add "Brownfield", "minecraft:podzol"
blocks.Add "AnalyteBox", "minecraft:barrel"
blocks.Add "Grass", "minecraft:grass_block"
blocks.Add "XRay", "minecraft:glass"
blocks.Add "Command", "minecraft:command_block"

' Call all command-generating functions
Call commandClearArea(blocks)
Call commandCreateArea(blocks, analyteBlocks, analytes, analyteNames, analyteMaxes, analyteStdevs, analyteBkgs, analyteBkgStdevs)
Call commandXRayBrownfield(blocks)
Call commandItemSoilProbe(blocks)
MsgBox("Soil sampling mod commands complete!")

'Dim value, i
'For i = 1 To 10
'    Randomize
'    value = Application.WorksheetFunction.NormInv(Rnd, 100, 5)
'    MsgBox (value)
'Next i
