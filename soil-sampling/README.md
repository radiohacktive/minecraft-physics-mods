## Minecraft Physics Mods: Soil Sampling

This mod creates a handful of commands to generate different models of a contaminated region, along with a soil boring tool with which to sample columns of blocks below the surface without digging up the surrounding area.


## Usage

You can use these mods in three easy steps:

* Open the MPM Excel workbook, enter your world name, and click the SETUP button
* Enter any additional customized info for the mod and click the RUN button
* Open your Minecraft world and run the `/reload` command in the chat


The mod dashboard provides the syntax and specific commands created.


## Commands

(TODO)

