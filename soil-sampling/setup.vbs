' Obtain world, function namespace, and folder names from Settings tab
Dim scriptdir: scriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
Set xlBook = GetObject("C:\Users\Radiohacktive\Programs\GitLab\minecraft-physics-mods\MPM_soil-sampling.xlsm")
Set xlApp = xlBook.Application
Dim worldname: worldname = xlApp.Range("worldname").value
Dim namespace: namespace = xlApp.Range("namespace").value
Dim modfolder: modfolder = xlApp.Range("modfolder").value


' Obtain home folder
Dim oShell, homepath: Set oShell = CreateObject("WScript.Shell")
homepath = oShell.ExpandEnvironmentStrings("%USERPROFILE%")

' Obtain Minecraft world folder
Dim worldpath: worldpath = homepath + "\AppData\Roaming\.minecraft\saves\" + worldname

' Obtain world functions folder; build as needed
Dim FSO: Set FSO = CreateObject("Scripting.FileSystemObject")
'
' datapacks/
Dim datapackspath: datapackspath = worldpath + "\datapacks"
If FSO.FolderExists(datapackspath) <> True Then
    FSO.CreateFolder (datapackspath)
End If
'
' namespace
Dim namespacepath: namespacepath = datapackspath + "\" + namespace
If FSO.FolderExists(namespacepath) <> True Then
    FSO.CreateFolder (namespacepath)
End If
'
' pack.mcmeta file
Dim packmcmetafilepath: packmcmetafilepath = namespacepath + "\pack.mcmeta"
If FSO.FileExists(packmcmetafilepath) <> True Then
    Dim packmcmetafile: Set packmcmetafile = FSO.OpenTextFile(packmcmetafilepath, 2, True)
    packmcmetafile.WriteLine ("{" + vbCrLf + "  ""pack"": {" + vbCrLf + "    ""description"": """", " + vbCrLf + "    ""pack_format"": 8" + vbCrLf + "  }" + vbCrLf + "}")
End If
'
' data/
Dim datapath: datapath = namespacepath + "\data"
If FSO.FolderExists(datapath) <> True Then
    FSO.CreateFolder (datapath)
End If
'
' modfolder
Dim modfolderpath: modfolderpath = datapath + "\" + modfolder
If FSO.FolderExists(modfolderpath) <> True Then
    FSO.CreateFolder (modfolderpath)
End If
'
' mcfunctions folder
Dim mcfunctionspath: mcfunctionspath = modfolderpath + "\functions"
If FSO.FolderExists(mcfunctionspath) <> True Then
    FSO.CreateFolder (mcfunctionspath)
End If

' Close system files
Set FSO = Nothing
 
' Return setup value
xlApp.Range("mcfunctionspath").value = mcfunctionspath
